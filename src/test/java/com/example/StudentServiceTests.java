package com.example;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import com.example.model.StudentModel;
import com.example.service.StudentService;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class StudentServiceTests {

	@Autowired
	StudentService service;

	@Test
	public void testSelectAllStudents() {
		List<StudentModel> students = service.selectAllStudents();

		Assert.assertNotNull("Gagal = student menghasilkan null", students);
		Assert.assertEquals("Gagal - size students tidak sesuai", 4, students.size());

	}

	@Test
	public void testSelectStudent(String npm) {
		StudentModel student = service.selectStudent(npm);

		Assert.assertNotNull("Gagal = student menghasilkan null", student);
		Assert.assertEquals("Gagal - Nama student tidak sesuai", "Anang", student.getName());
		Assert.assertEquals("Gagal - GPA student tidak sesuai", 3.5, student.getGpa(), 0.0);
	}

	@Test
	public void testCreateStudent() {
		StudentModel student = new StudentModel("126", "Budi", 3.44, null);
		// Cek apakah student sudah ada
		Assert.assertNull("Mahasiswa sudah ada", service.selectStudent(student.getNpm()));
		// Masukkan ke service
		service.addStudent(student);
		// Cek apakah student berhasil dimasukkan
		Assert.assertNotNull("Mahasiswa gagal dimasukkan", service.selectStudent(student.getNpm()));
	}
	
	@Test
	public void testUpdateStudent(String npm) {
		StudentModel student = service.selectStudent(npm);
		Assert.assertNotNull("Gagal = student menghasilkan null", student);
		student.setGpa(3.9);
		service.updateStudent(student);
		Assert.assertEquals("Gagal - GPA tidak sesuai", 3.9, service.selectStudent(npm).getGpa(), 0.0);
	}
	
	@Test
	public void testDeleteStudent(String npm) {
		StudentModel student = service.selectStudent(npm);
		Assert.assertNotNull("Gagal = student menghasilkan null", student);
		service.deleteStudent(npm);
		Assert.assertNull("Gagal - tidak berhasil delete", service.selectStudent(npm));
	}

}
